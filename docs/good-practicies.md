# Bonnes pratiques


## Features
Creer une branche features, à partir de dev pour chaque fonctionnalité "feature/{fonctionalité}

## Hotfix
Pour regler un problème, creer une nouvelle branche hotfix/{numéro du ticket}

## Messages des commit
Utiliser GitMoji

Utiliser des emoji pour sééparer les commit par catégories :
⚡= update performance
🐞= bugfix
⚗️ = unit Test
✨ = new feature
Mettres des messages de commit clair et en anglais

