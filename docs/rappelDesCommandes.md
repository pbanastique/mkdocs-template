# Rappel des commandes



<div id='whatismarkdown'/>  

## Les commandes locales 

### git add
| Commande| Desciption|
| ------|-----|
| git add .| |

###git commit
| Commande| Desciption|
| ------|-----|
| git commit -m *"Message précis"*| |
| git commit -am "Message précis"| |

### git config
| Commande| Desciption|
| ------|-----|
| git  config --global| |
| git  config --system| |

### git log

### git rm
| Commande| Desciption|
| ------|-----|
| git rm --cached <fichier>| Supprime un fichier de l'index |
| git rm - r --cached <dossier>| |

### git status

## Les commandes collaboratives
### git push
### git pull

## Le fichier .gitignore

Le fichier doit être encodé en **UTF-8**