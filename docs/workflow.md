# Workflow

Nous utilisons chez rossignol 2 types de workflow :

## Centralisé

Le workflow centralisé est utilisé pour les developpement avec un seul dev.
Cela permet juste d'avoir un historique du projet.

![image](images/workflow-centralisé.png)


## Gitflow

Le workflow Gitfow est utilisé pour les projet avec plusieurs developpeurs :

Nous avons 3 branches par défaut : dev,tst et prod

Des CIs sont misent en place pour déployer automatiquement sur les différents environnements.
Pour déclancher ses déploiement, il faut faire des pulls request d'une branche vers une autre.


![image](images/workflow-gitflow.png)
