# GIT POUR LES DEVS 
![](http://i.imgur.com/IMTN5cy.png)  

Bonjour, <br/>
Dans ce tutoriel, vous apprendrez les bases de GIT, nouveau gestionnaire de contrôle source à ROSSIGNOL.  

*******
Table des matières  
 1. [Rappel des commandes](#commandReminder)
 2. [Exemples](#examples)
 3. [Les différents workflows](#workflows)
 4. [Les bonnes pratiques](#BestPractices)

*******

<div id='commandReminder'/>  

## Quelles sont les principales commandes de GIT ?  
 

  >*Vous trouverez les principales <a href="rappelDesCommandes/">commandes GIT</a> *   

<div id='examples'/>  

## Exemples d'utilisation de GIT ?  

  >*Vous trouverez des exemples d'utilisation de GIT*

 
<div id='workflows'/>  

## Les différents Workflows

<a href="workflow/">Workflows</a>

 - Workflow centralized

 - Workflow Git lab Flow


<div id='BestPractices'/>  

## Les bonnes pratiques de git

  >*Vous trouverez ici les <a href="good-practicies/">bonnes pratiques</a> de git*



